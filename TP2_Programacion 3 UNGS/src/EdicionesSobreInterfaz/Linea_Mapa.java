package EdicionesSobreInterfaz;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
import Interfaz.VentanaGrafo;

public class Linea_Mapa 
{	
	public static void trazar_linea (double x1, double y1, double x2, double y2, Color color) 
	{
		Coordinate one = new Coordinate(x1, y1);
		Coordinate two = new Coordinate(x2, y2);
		List<Coordinate> coordenadas = new ArrayList<Coordinate>();
		coordenadas.add(one); 
		coordenadas.add(two); 
		coordenadas.add(two);
		MapPolygon linea = new MapPolygonImpl(coordenadas);
		linea.getStyle().setColor(color);
		VentanaGrafo.mapa.addMapPolygon(linea);

	}
	
	public static void pesoArista(double x1, double y1, double x2, double y2, Integer peso) 
	{
		Coordinate mid = new Coordinate(((x2-x1)/2+x1), ((y2-y1)/2+y1));
		MapMarker marker1 = new MapMarkerDot(peso.toString(), mid);
		setMarker(marker1);
	}
	
	public static void setMarker(MapMarker marker1) 
	{
		Color color = new Color(0,0,0,0);
		marker1.getStyle().setBackColor(color);
		marker1.getStyle().setColor(color);
		VentanaGrafo.mapa.addMapMarker(marker1);
	}
}
