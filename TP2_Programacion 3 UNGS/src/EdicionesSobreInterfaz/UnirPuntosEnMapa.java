package EdicionesSobreInterfaz;

import Interfaz.Ingreso_Localidad;
import Interfaz.VentanaGrafo;
import CodigoNegocio.Grafo;
import CodigoNegocio.Logica;

public class UnirPuntosEnMapa {
	
	public  static VentanaGrafo ventana = Ingreso_Localidad.ventana; 

	
	public static  void UnirPuntosIngresados(Grafo grafo, int[][]Matriz, int tamanio, 
			int[][]Matriz_peso_aristas, double[]coordenada_x, 
			double[]coordenada_y, String[] nom, int aristaMayor)
	{	
	    for (int i = 0; i < Matriz.length; i++) 
	    {
	    	nom[i]= Grafo.localidades.get(i).getNombre_localidad();
	    	grafo.setCordeX(i, coordenada_x[i]);
	        grafo.setCordeY(i, coordenada_y[i]);
	        grafo.setNombre(i, nom[i]);
	    	for (int j = 0; j < Matriz.length; j++) 
	    	{
				if(i==j) 
				{
					Matriz[i][j] = 0;
	    			Matriz_peso_aristas[i][j] = 0;
				}
				else 
				{
					Matriz[i][j] = 1;
					int distancia = Logica.distanciaCoord(coordenada_x[i], coordenada_y[i],coordenada_x[j],coordenada_y[j]);				 
					Matriz_peso_aristas[i][j] = distancia;
	    			Linea_Mapa.pesoArista(coordenada_x[i], coordenada_y[i], coordenada_x[j],coordenada_y[j], distancia);
	    			ventana.setAristaMayor(distancia);
				}
			}
		}
	    	    
	    for (int j = 0; j < tamanio; j++) 
	    {            
	    	for (int k = 0; k < tamanio; k++) 
	    	{
	        	grafo.setMatrizAdyacencia(j,k, Matriz[j][k]);
	            grafo.setMatrizAristas(j, k, Matriz_peso_aristas[j][k]); 
	        }
	    }        
	    
	}


}
