package EdicionesSobreInterfaz;

import static Interfaz.VentanaGrafo.ingresarLocalidadOrigen;
import static Interfaz.VentanaGrafo.jPanel1;
import java.awt.Color;
import CodigoNegocio.Grafo;

public class Algoritmo_Prim 
{    
   private int kilometros_Acumulados;
   private int aristaMenor;
   private int  fin;
   private boolean existeLocalidad;
   private boolean aumentaTamano;
   private int verticeApuntado;  
   private int verticeApuntador;
   private int tamano;
   private int aristaMayor;
   private int cantVertices;
   private int nodoOrigen;
   private Grafo grafo;
       
   public Algoritmo_Prim(Grafo graf , int vertices ,int _aristaMayor )
   {
       this.kilometros_Acumulados = 0;
       this.aristaMenor = 0;
       this.fin = 0;
       this.existeLocalidad=false;
       this.aumentaTamano = false;
       this.verticeApuntado = 0;  
       this.verticeApuntador = 0;
       this.tamano = 1;
       this.aristaMayor=_aristaMayor;
       this.grafo = graf;
       this.cantVertices = vertices;
   }
   
    public void prim()
    {
    	this.nodoOrigen= ingresarLocalidadOrigen("Ingrese el n� localidad de Origen","Localidad no existe",cantVertices);
    	jPanel1.paint(jPanel1.getGraphics());
    	grafo.crearEnGrafo(cantVertices);
    	grafo.setEnGrafo(0, nodoOrigen);
    	
    	while(fin<2)
    	{
    		this.aristaMenor = this.aristaMayor;
    		this.fin=2;
            for (int j = 0; j < tamano; j++) 
            {
                for (int k = 0; k < cantVertices; k++)
                {
                    if(grafo.getMatriz_Adyacencia(k, grafo.getEnGrafo(j))==1)
                    {
                        for (int h = 0; h < tamano; h++) 
                        {
                             if(grafo.getEnGrafo(h)==k )
                             {
                                 this.existeLocalidad=true; 
                                 break;
                             }
                        }
                        if(!existeLocalidad)
                        {
                            if(grafo.getMatriz_aristas(k, grafo.getEnGrafo(j))<=aristaMenor && grafo.getMatriz_aristas(k, grafo.getEnGrafo(j))>0 )
                            {
                                 aristaMenor=grafo.getMatriz_aristas(k, grafo.getEnGrafo(j));
                                 this.verticeApuntado=k;
                                 this.aumentaTamano=true;
                                 this.verticeApuntador=grafo.getEnGrafo(j);
                                 this.fin=1;                    
                            }
                        }
                        this.existeLocalidad=false;
                    }                  
                }
            }           
            if(aumentaTamano==true)
            {
            	Linea_Mapa.trazar_linea(grafo.getCordeX(verticeApuntador), grafo.getCordeY(verticeApuntador),grafo.getCordeX(verticeApuntado), grafo.getCordeY(verticeApuntado), Color.red);
        	 	grafo.setEnGrafo(tamano, verticeApuntado);
        	 	this.tamano++;
        	 	this.aumentaTamano=false;
        	 	this.kilometros_Acumulados += this.aristaMenor;
         	}
        }
    }

    public int getKilometros_acumulados() 
    {
        return kilometros_Acumulados;
    }
}
