package TestCodigoNegocio;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import CodigoNegocio.Grafo;
import CodigoNegocio.Localidad;

public class GrafoTest 
{

	@Test(expected = IllegalArgumentException.class)	
	public void PesoNegativoArista_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setMatrizAristas(0, 1, -5);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceAristaExcedido_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setMatrizAristas(0, 5, 5);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void MatrizAdyacenciaNoBinaria_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setMatrizAdyacencia(0, 1, 2);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceMAdyacenciaExcedida_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setMatrizAdyacencia(0, 5, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceNegativo_cordeX_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setCordeX(-2, 34.62);	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceExcedido_cordeX_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setCordeX(5, 34.62);	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceNegativo_cordeY_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setCordeY(-2, 34.62);	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceExcedido_cordeY_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setCordeY(5, 34.62);	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceNegativo_nombre_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setNombre(-2, "Malvinas Argentina");	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceExcedido_nombre_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setNombre(5, "Los Polvorines");	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceNegativo_enGrafo_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setEnGrafo(-2, 4);	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceExcedido_enGrafo_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.setEnGrafo(5, 4);	
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void IndiceNegativo_CrearEnGrafo_test() 
	{
		Grafo grafo = new Grafo(5);
		grafo.crearEnGrafo(-1);	
	}
	
	@Test
	public void agregarLocalidad_test()
	{
		Grafo grafo = new Grafo(5);
		
		Localidad sanMiguel = new Localidad("San Miguel","Bs As", -34.361 , -58.276);
		Localidad joseCpaz = new Localidad("Jose C Paz","Bs As", -34.373 , -58.265);
		Localidad pilar = new Localidad("Pilar","Bs As",-34.003 , -58.209);

		grafo.agregarLocalidad(sanMiguel);
		grafo.agregarLocalidad(joseCpaz);
		grafo.agregarLocalidad(pilar);
		
		assertTrue( Grafo.localidades.get(0).getNombre_localidad().equals("San Miguel") );
		assertTrue( Grafo.localidades.get(1).getNombre_localidad().equals("Jose C Paz") );
		assertTrue( Grafo.localidades.get(2).getNombre_localidad().equals("Pilar") );
	}
	
	@Test
	public void agregarDosVecesMismaLocalidad_test()
	{
		Grafo grafo = new Grafo(5);
		
		Localidad sanMiguel = new Localidad("San Miguel","Bs As", -34.361 , -58.276);
		Localidad sanMiguel_2 = new Localidad("San Miguel","Bs As", -34.361 , -58.276);

		grafo.agregarLocalidad(sanMiguel);
		grafo.agregarLocalidad(sanMiguel_2);
		
		assertFalse( Grafo.localidades.get(1).getNombre_localidad().equals("San Miguel") );
	}
}
