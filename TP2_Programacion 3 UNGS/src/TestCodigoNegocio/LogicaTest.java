package TestCodigoNegocio;

import static org.junit.Assert.*;
import org.junit.Test;
import CodigoNegocio.Logica;

public class LogicaTest {

	@Test 
	public void distanciaCoord_test() {
		int res = Logica.distanciaCoord(1, 1, 1, 3);
		assertEquals(222,res);
	}
	
	@Test 
	public void condicionesPrecio_test() {
		//100 = costo fijo
		int res = Logica.condicionesPrecio(299, false);
		assertEquals(res, 299*100);
	}
	
	@Test 
	public void condicionesPrecio_excede300km_test() {
		//50 = costo si excede los 300 km
		int res = Logica.condicionesPrecio(300001, false);
		assertEquals(res, 300001 * 50);
	}
	
	@Test 
	public void condicionesPrecio_LocalidadesIguales_test() {
		//200 = costo por localidades iguales
		int res = Logica.condicionesPrecio(299, true);
		assertEquals(res, 200*299);
	}
	
	@Test 
	public void condicionesPrecio_LocIguales_ExcedeKM_test() {
		//50 = excede los 300 km, 200 = localidades iguales
		int res = Logica.condicionesPrecio(300001, true);
		assertEquals(res, 300001*(50+200));
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void setCostoTotalNegativo_test() 
	{
		Logica.setCostoTotal(1 , -1);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void setCostoTotal_KM_Negativo_test() 
	{
		Logica.setCostoTotal(-1 , 123);
	}	
}
