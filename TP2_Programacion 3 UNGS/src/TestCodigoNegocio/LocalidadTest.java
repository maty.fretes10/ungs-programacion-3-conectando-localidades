package TestCodigoNegocio;

//import static org.junit.Assert.*;
import CodigoNegocio.Localidad;
import org.junit.Test;

public class LocalidadTest 
{
	@Test(expected = IllegalArgumentException.class)	
	public void nombreLocalidad_noVacio_test() 
	{
		Localidad local = new Localidad("Mu�iz","Buenos Aires",-34.315,-58.357);
		local.setNombre_localidad("");
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void nombreProvincia_noVacio_test() 
	{
		Localidad local = new Localidad("San Miguel","Buenos Aires",-34.315,-58.357);
		local.setNombre_provincia("");
	}
	
//	@Test
//	public void localidad_bienIngresada_test() {
//		Localidad local = new Localidad("Bella Vista","Buenos Aires",-34.907,-58.708);
//		assertTrue(local.getNombre_localidad().equals("Bella Vista"));
//		assertTrue(local.getNombre_provincia().equals("Buenos Aires"));
//		assertTrue(local.getLatitud() == -34.907);
//		assertTrue(local.getLongitud() == -58.708);
//	}
	
	
}
