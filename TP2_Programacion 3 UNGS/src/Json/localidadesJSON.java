package Json;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import CodigoNegocio.Localidad;

public class localidadesJSON 
{
	private ArrayList<Localidad> localidades;

	public localidadesJSON() 
	{
		localidades = new ArrayList<Localidad>();
	}
	
	public void aņadirLocalidad(Localidad _local) 
	{
		localidades.add(_local);
	}
	
	public String generarJSONPretty() 
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		return json;
	}
	
	public void guardarJSON(String jsonParaGuardar, String archivoDestino)
	{
		try
		{
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static localidadesJSON leerJSON(String archivo)
	{
		Gson gson = new Gson();
		localidadesJSON ret = null;
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, localidadesJSON.class);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	public ArrayList<Localidad> getLocalidades() {
		return localidades;
	}

	public void setLocalidades(ArrayList<Localidad> localidades) {
		this.localidades = localidades;
	}
}



