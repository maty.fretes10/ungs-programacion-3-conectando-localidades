package Interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import CodigoNegocio.Grafo;
import CodigoNegocio.Logica;
import EdicionesSobreInterfaz.Algoritmo_Prim;
import EdicionesSobreInterfaz.Linea_Mapa;
import EdicionesSobreInterfaz.UnirPuntosEnMapa;

import java.awt.Color;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JList;
import javax.swing.UIManager;

public class VentanaGrafo 
{
	JFrame frame;
	private int aristaMayor;
	public static JPanel jPanel1;	
	public static Grafo grafo = new Grafo(51);    
	public static int Cant_vertices_creados;
	public static JMapViewer mapa;
	private ArrayList<Coordinate> coordenadas = new ArrayList<>();
	private double[] latitudes = new double[51];;
	private double[] longitudes= new double[51];
	private Algoritmo_Prim Prim;

	/**
	 * Create the application.
	 */
	public VentanaGrafo() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{	
		frame = new JFrame();
		frame.setTitle("CONEXION DE LOCALIDADES");
		frame.setBounds(100, 100, 723, 456);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		jPanel1 = new JPanel();
		jPanel1.setBackground(Color.LIGHT_GRAY);
		jPanel1.setBounds(166, 10, 521, 399);
		frame.getContentPane().add( jPanel1);
		
		mapa= new JMapViewer();
		mapa.setBounds(0, 0, 521, 399);
		Coordinate cooredenada = new Coordinate(-34.546, -58.719);
		mapa.setDisplayPosition(cooredenada, 12);

		MapPolygon poligono = new MapPolygonImpl(coordenadas);
		mapa.addMapPolygon(poligono);
		jPanel1.setLayout(null);
		jPanel1.add(mapa);
		
		JTextField Peso_acumulado = new JTextField();
		Peso_acumulado.setEditable(false);
		Peso_acumulado.setBounds(34, 172, 96, 19);
		frame.getContentPane().add(Peso_acumulado);
		Peso_acumulado.setColumns(10);

		JButton Boton_grafo = new JButton("GRAFO");
		Boton_grafo.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) 
        	{
        		Accion_generarGrafo();
        	}
        });
		Boton_grafo.setBounds(34, 33, 85, 21);
		frame.getContentPane().add(Boton_grafo);
		
		JButton Boton_prim = new JButton("PRIM");
		Boton_prim.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) 
        	{
        		Item_algoritmo_prim(Peso_acumulado);
        	}
        });
		Boton_prim.setBounds(34, 59, 85, 21);
		frame.getContentPane().add(Boton_prim);
				
		JLabel lblPeso_aristas = new JLabel("Kilometros: ");
		lblPeso_aristas.setBounds(34, 153, 78, 19);
		frame.getContentPane().add(lblPeso_aristas);
		
		JButton Boton_costos = new JButton("COSTOS");
		Boton_costos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				resultados();
			}
		});
		Boton_costos.setBounds(34, 89, 85, 21);
		frame.getContentPane().add(Boton_costos);
		
		JList<String> list = new JList<String>();
		list.setBackground(UIManager.getColor("CheckBox.background"));
		list.setBounds(34, 206, 96, 106);
		DefaultListModel <String> _modeloLista = Ingreso_Localidad.modeloLista;
		list.setModel(_modeloLista);
		frame.getContentPane().add(list);
		
		JButton btnNewButton = new JButton("DATA");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				data();
			}
		});
		btnNewButton.setBounds(34, 120, 85, 21);
		frame.getContentPane().add(btnNewButton);
		
	}
	boolean provinciasdistintas = false;
	public void resultados() 
	{
		for (int i = 0; i < Grafo.localidades.size(); i++) {
			for (int j = 0; j < Grafo.localidades.size(); j++) {
				if(!Grafo.localidades.get(i).getNombre_provincia().equals(Grafo.localidades.get(j).getNombre_provincia()))
					provinciasdistintas = true;
			}
		}
		JOptionPane.showMessageDialog(null, "El costo total es: "+ Logica.condicionesPrecio(Prim.getKilometros_acumulados(), provinciasdistintas));		
	}
	
	public void data() 
	{
		JOptionPane.showMessageDialog(null,"Excede los 300 km: "+ Logica.getExcedio300Km()+"\n"+"Interconexion de provincias: "+Logica.getInterconxionProvincias());
	}

	public static int ingresarLocalidadOrigen(String origen, String noExiste,int CantVertices)
	{
	    int nOrigen = 0;
	        try
	        {
	            nOrigen = Integer.parseInt(JOptionPane.showInputDialog(""+origen));   
	            if(nOrigen >= CantVertices)
	            {  
	                  JOptionPane.showMessageDialog(null,""+noExiste+"\nDebe ingresar el n� de una localidad existente");
	                  nOrigen = ingresarLocalidadOrigen(origen,noExiste, CantVertices);
	            }
	        }
	        catch(Exception ex)
	        {
	            nOrigen = ingresarLocalidadOrigen(origen,noExiste,CantVertices);
	        }
	    return nOrigen;
	}        
	        
	private void Accion_generarGrafo() 
	{	
	    int tamanio = coordenadas.size();  
	    int[][] Matriz = new int [tamanio][tamanio];
	    int[][] Matriz_peso_aristas = new int [tamanio][tamanio];
	    double[] coordenada_x = latitudes;
        double[] coordenada_y= longitudes;
        String[] nom = new String[tamanio];          
        
	    UnirPuntosEnMapa.UnirPuntosIngresados(grafo, Matriz, tamanio, Matriz_peso_aristas, coordenada_x, coordenada_y, nom, getAristaMayor());
	    	    
	    Cant_vertices_creados=tamanio; 
	    TrazarLineasEnMapa(Cant_vertices_creados,grafo);
	}

	private void Item_algoritmo_prim(JTextField km_recorrido) 
	{
		if(Cant_vertices_creados<1)
			JOptionPane.showMessageDialog(null,"Aun no se ha creado Un nodo");     
		else
		{
			Prim = new Algoritmo_Prim(grafo,Cant_vertices_creados, getAristaMayor());
			Prim.prim();
			km_recorrido.setText(""+Prim.getKilometros_acumulados());
		}         
	}

	public static void TrazarLineasEnMapa(int tope, Grafo _grafo)
	{ 
		for (int j = 0; j < tope; j++) 
		{
	        for (int k = 0; k < tope; k++) 
	        {
	            if(_grafo.getMatriz_Adyacencia(j, k) == 1)        	
	            	Linea_Mapa.trazar_linea(_grafo.getCordeX(j),_grafo.getCordeY(j), _grafo.getCordeX(k), _grafo.getCordeY(k), Color.blue);
	        }
	    }	            
	}

	public ArrayList<Coordinate> getCoordenadas() 
	{
		return coordenadas;
	}

	public void setCoordenadas(String nombre, double coorX, double coorY) 
	{
		latitudes[coordenadas.size()] = coorX;
		longitudes[coordenadas.size()] = coorY;
		Coordinate coord = new Coordinate(coorX, coorY);
		MapMarker marker1 = new MapMarkerDot(nombre, coord);
		marker1.getStyle().setBackColor(Color.red);
		marker1.getStyle().setColor(Color.orange);
		mapa.addMapMarker(marker1);	
		coordenadas.add(coord);
	}
	
	public int getAristaMayor() 
	{
		return aristaMayor;
	}

	public void setAristaMayor(int valor) 
	{
		if(aristaMayor < valor) 
			aristaMayor = valor;
	}
}
