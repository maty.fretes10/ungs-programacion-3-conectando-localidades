package Interfaz;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import CodigoNegocio.Grafo;
import CodigoNegocio.Localidad;
import CodigoNegocio.Logica;
import Json.localidadesJSON;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.SystemColor;
import javax.swing.JTextPane;

public class Ingreso_Localidad 
{
	public JFrame frame;
	public Grafo grafo = new Grafo(51);
	public static DefaultListModel <String> modeloLista = new DefaultListModel<String>();
	private Logica log = new Logica();
	public static VentanaGrafo ventana = new VentanaGrafo();
	localidadesJSON local = new localidadesJSON();
	int contador;

	/**
	 * Create the application.
	 */
	public Ingreso_Localidad() 
	{
		initialize();	
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{	
		frame = new JFrame();
		frame.setBounds(100, 100, 604, 501);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_localidad = new JLabel("LOCALIDAD");
		lbl_localidad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbl_localidad.setBounds(46, 45, 100, 34);
		frame.getContentPane().add(lbl_localidad);
		
		JLabel lbl_provincia = new JLabel("PROVINCIA");
		lbl_provincia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbl_provincia.setBounds(46, 89, 100, 34);
		frame.getContentPane().add(lbl_provincia);
		
		JLabel lbl_latitud = new JLabel("LATITUD");
		lbl_latitud.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbl_latitud.setBounds(46, 133, 100, 34);
		frame.getContentPane().add(lbl_latitud);
		
		JLabel lbl_longitud = new JLabel("LONGITUD");
		lbl_longitud.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbl_longitud.setBounds(46, 177, 100, 34);
		frame.getContentPane().add(lbl_longitud);
			
		JTextField txt_localidad = new JTextField();
		txt_localidad.setFont(new Font("Tahoma", Font.PLAIN, 17));
		txt_localidad.setBounds(174, 45, 167, 29);
		frame.getContentPane().add(txt_localidad);
		txt_localidad.setColumns(10);
		
		JTextField txt_provincia = new JTextField();
		txt_provincia.setFont(new Font("Tahoma", Font.PLAIN, 17));
		txt_provincia.setColumns(10);
		txt_provincia.setBounds(174, 94, 167, 29);
		frame.getContentPane().add(txt_provincia);
		
		JTextField txt_latitud = new JTextField();
		txt_latitud.setFont(new Font("Tahoma", Font.PLAIN, 17));
		txt_latitud.setColumns(10);
		txt_latitud.setBounds(174, 133, 167, 29);
		frame.getContentPane().add(txt_latitud);
		
		JTextField txt_longitud = new JTextField();
		txt_longitud.setFont(new Font("Tahoma", Font.PLAIN, 17));
		txt_longitud.setColumns(10);
		txt_longitud.setBounds(174, 177, 167, 29);
		frame.getContentPane().add(txt_longitud);
		
		JList <String> JList_localidades = new JList<String>();
		JList_localidades.setBackground(SystemColor.control);
		JList_localidades.setBounds(390, 45, 167, 165);
		frame.getContentPane().add(JList_localidades);
		JList_localidades.setModel(modeloLista);

		JButton boton_agregar = new JButton("AGREGAR");
		boton_agregar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) 
			{	
				String nombre_localidad = txt_localidad.getText();
				String nombre_provincia = txt_provincia.getText();	
				double x = Double.parseDouble(txt_latitud.getText());
				double y = Double.parseDouble(txt_longitud.getText());
				
			    grafo.agregarLocalidad(new Localidad(nombre_localidad,nombre_provincia,x,y));

			    //json
			    local.aņadirLocalidad(new Localidad(nombre_localidad,nombre_provincia,x,y));
			    
			    ventana.setCoordenadas(nombre_localidad,x,y);
			    agregarElementoalModeloLista(nombre_localidad.toUpperCase());
			    txt_localidad.setText("");
			    txt_provincia.setText("");
			    txt_latitud.setText("");
			    txt_longitud.setText("");
				
			}
		});
		boton_agregar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		boton_agregar.setBounds(107, 245, 119, 29);
		frame.getContentPane().add(boton_agregar);
		
		JButton btnVerEnMapa = new JButton("VER EN MAPA");
		btnVerEnMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				ventana.frame.setVisible(true);
				frame.setVisible(false);
				
				//json
				String jsonPretty = local.generarJSONPretty();
				local.guardarJSON(jsonPretty, "Coordenadas.JSON");
			}
		});
		btnVerEnMapa.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnVerEnMapa.setBounds(267, 245, 134, 29);
		frame.getContentPane().add(btnVerEnMapa);
		
		JLabel lblNewLabel = new JLabel("Localidades ingresadas");
		lblNewLabel.setBounds(390, 22, 167, 13);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lbl_precios = new JLabel("COSTOS:");
		lbl_precios.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbl_precios.setBounds(46, 314, 100, 34);
		frame.getContentPane().add(lbl_precios);
		
		JLabel lbl_precios_1 = new JLabel("Costo Fijo: ");
		lbl_precios_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lbl_precios_1.setBounds(126, 341, 100, 29);
		frame.getContentPane().add(lbl_precios_1);
		
		JLabel lbl_precios_300 = new JLabel("Costo m\u00E1s 300 km: ");
		lbl_precios_300.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lbl_precios_300.setBounds(126, 381, 126, 29);
		frame.getContentPane().add(lbl_precios_300);
		
		JLabel lbl_precios_prov = new JLabel("Costo interprovincial: ");
		lbl_precios_prov.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lbl_precios_prov.setBounds(126, 422, 134, 29);
		frame.getContentPane().add(lbl_precios_prov);
		
		JTextPane textPane_fijo = new JTextPane();
		textPane_fijo.setBounds(253, 341, 38, 20);
		textPane_fijo.setText(log.getCostoFijo().toString());
		frame.getContentPane().add(textPane_fijo);
		
		JTextPane textPane_300 = new JTextPane();
		textPane_300.setBounds(253, 381, 38, 20);
		textPane_300.setText(log.getCosto300km().toString() + "%");
		frame.getContentPane().add(textPane_300);
		
		JTextPane textPane_prov = new JTextPane();
		textPane_prov.setBounds(253, 422, 38, 20);
		textPane_prov.setText(log.getCostoDos().toString());
		frame.getContentPane().add(textPane_prov);
		
		JButton btnCargarSesion = new JButton("Cargar sesion anterior\r\n");
		btnCargarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Localidad> listolocal = localidadesJSON.leerJSON("Coordenadas.JSON").getLocalidades();
				
				if(listolocal.size() == 0) 
					JOptionPane.showMessageDialog(null, "No hay carga anterior");		
				
				for (int i = 0; i < listolocal.size(); i++) 
				{
					Localidad localidad = listolocal.get(i);
					grafo.agregarLocalidad(localidad);
					ventana.setCoordenadas(localidad.getNombre_localidad(),localidad.getLatitud(),localidad.getLongitud());
					agregarElementoalModeloLista(localidad.getNombre_localidad().toUpperCase());
				}
			}
		});
		btnCargarSesion.setBounds(174, 284, 167, 21);
		frame.getContentPane().add(btnCargarSesion);
	}
	
	public void agregarElementoalModeloLista(String nombre) 
	{
		modeloLista.addElement(contador++ +" : " + nombre);
	}
	
}
