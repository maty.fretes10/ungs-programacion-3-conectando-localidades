package CodigoNegocio;

import java.util.ArrayList;

public class Grafo 
{	   
	private Integer[][] matriz_aristas;
	private Integer[][] matriz_Adyacencia;
	private double[] cordeX;
	private double[] cordeY;
	private String[] nombre;
	private Integer[] enGrafo;
	public int _cantidad; 
	public static ArrayList<Localidad> localidades;
	   	
   	public Grafo(int cantidad)
   	{
        matriz_aristas = new Integer[cantidad][cantidad];
        matriz_Adyacencia= new Integer[cantidad][cantidad];
        cordeX = new double[cantidad];
        cordeY = new double[cantidad];
        nombre = new String[cantidad];
        set_cantidad(cantidad);
   	}
	
   	public void agregarLocalidad(Localidad local) 
	{
   		if(localidades == null) 
   		{
   			localidades = new ArrayList<>();
   		}
   		localidades.add(local);
   	}

    public void setMatrizAristas(int i,int j ,int m_aristas) 
    {
    	if(i >= get_cantidad() || j >= get_cantidad())
    		throw new IllegalArgumentException("Limite excedido");	
    	if(i == j)
    		this.matriz_aristas[i][j] = 0;
    	if(m_aristas >= 0)
    		this.matriz_aristas[i][j] = m_aristas;
    	else
    		throw new IllegalArgumentException("Los pesos de las aristas deben ser positivos");
    }

    public void setMatrizAdyacencia(int i,int j , int m_Adyacencia) 
    {
    	if(i >= get_cantidad() || j >= get_cantidad())
    		throw new IllegalArgumentException("Limite excedido");	
    	if(i == j) 
    		this.matriz_Adyacencia [i][j] = 0;
		if(m_Adyacencia == 0 || m_Adyacencia == 1)
    		this.matriz_Adyacencia[i][j] = m_Adyacencia;
		else
			throw new IllegalArgumentException("Matriz binaria, solo acepta 0 y 1");
	}

    public void setCordeX(int i,double cordeX) 
    {
    	if (i>=0 && i<get_cantidad())
    		this.cordeX[i] = cordeX;
    	else 
    		throw new IllegalArgumentException("Indice incorrecto");
    }

    public void setCordeY(int i, double cordeY) 
    {
    	if (i>=0 && i<get_cantidad())
    		this.cordeY[i] = cordeY;
    	else 
    		throw new IllegalArgumentException("Indice incorrecto");
    }

	public void setNombre(int i,String _nombre) 
	{
		if (i>=0 && i<get_cantidad())
			this.nombre[i] = _nombre;
	    else 
    		throw new IllegalArgumentException("Indice incorrecto");
	}

    public void setEnGrafo(int i,int enArbol) 
    {
    	if (i>=0 && i<get_cantidad())
    		this.enGrafo[i] = enArbol;
        else 
    		throw new IllegalArgumentException("Indice incorrecto");
    }
    
    public void crearEnGrafo(int i)
    {
    	if (i>=0)
    		enGrafo = new Integer [i]; 
        else 
    		throw new IllegalArgumentException("No se crea el arreglo con un negativo");
    }

	public void set_cantidad(int _cantidad) 
	{
		this._cantidad = _cantidad;
	}
	
   	//GETTERS
   	public ArrayList<Localidad> getLocalidad()
   	{
   		return localidades;
   	}
   	
	public int getMatriz_aristas(int i, int j ) 
	{
        return matriz_aristas[i][j];
    }

    public int getMatriz_Adyacencia(int i,int j) 
    {
        return matriz_Adyacencia[i][j];
    }

    public double getCordeX(int i) 
    {
        return cordeX[i];
    }

    public double getCordeY(int i) 
    {
        return cordeY[i];
    }

    public String getNombre(int i) 
    {
        return nombre[i];
    }

    public int getEnGrafo(int i) 
    {
        return enGrafo[i];
    }
    
    public int get_cantidad() 
   	{
		return _cantidad;
	}
}

