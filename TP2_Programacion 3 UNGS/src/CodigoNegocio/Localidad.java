package CodigoNegocio;

public class Localidad 
{    
	   private String nombre_localidad;
	   private String nombre_provincia;
	   private double latitud;
	   private double longitud;
	   
	   public Localidad(String localidad, String provincia, double latitud, double longitud)
	   {
	       this.nombre_localidad = localidad;
	       this.nombre_provincia = provincia;
	       this.latitud = latitud;
	       this.longitud = longitud;
	   }

		public String getNombre_localidad() 
		{
			return nombre_localidad;
		}

		public void setNombre_localidad(String _nombre_localidad) 
		{
			if(nombre_localidad.length()<1)
				this.nombre_localidad = _nombre_localidad;
			else
				throw new IllegalArgumentException("Ingrese el nombre de la localidad");
		}

		public String getNombre_provincia() 
		{
			return nombre_provincia;
		}

		public void setNombre_provincia(String _nombre_provincia) 
		{
			if(nombre_provincia.length()<1)
				this.nombre_provincia = _nombre_provincia;
			else
				throw new IllegalArgumentException("Ingrese el nombre de la localidad");
		}

		public double getLatitud() 
		{
			return latitud;
		}

		public void setLatitud(double latitud) 
		{
			this.latitud = latitud;
		}

		public double getLongitud() 
		{
			return longitud;
		}

		public void setLongitud(double longitud) 
		{
			this.longitud = longitud;
		}
}
