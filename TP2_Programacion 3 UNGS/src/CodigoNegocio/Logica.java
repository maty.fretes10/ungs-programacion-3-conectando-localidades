package CodigoNegocio;

public class Logica 
{
	private  static int costo300km = 50;
	private  static int costoDos = 200;
	private  static int costoFijo = 100;
	private static int costoTotal;
	private static boolean excedioKm;
	private static boolean inteconectaDosPrivincias;

	public static int condicionesPrecio(int km, boolean provinciasdistintas) 
	{
		//dos provincias distintas: true y Excede km: false
		if(provinciasdistintas && km < 300)
		{
			setCostoTotal(km, costoDos);
			setInteconexionProvincias(true);
		}
		
		//dos provincias distintas: false y Excede km: true
		if(km > 300 && !provinciasdistintas) 
		{
			setCostoTotal(km, costo300km);
			setExcedio300Km(true);
		}
		
		//dos provincias distintas: false y Excede km: false
		if(km<300 && !provinciasdistintas) 
		{
			setCostoTotal(km, costoFijo);
		}
		
		////dos provincias distintas: true y Excede km: true
		if(km>300 && provinciasdistintas) 
		{
			setCostoTotal(km, costo300km+costoDos);
			setExcedio300Km(true);
			setInteconexionProvincias(true);
		}
		
		return getCostoTotal();
	}	
	

   	public static int distanciaCoord(double lat1, double lng1, double lat2, double lng2) 
	{   
        double radioTierra = 6371;//en kilómetros  
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        int distancia = (int) (radioTierra * va2);  
   
        return distancia;  
    }
	
	public static void setCostoTotal(int km, int _costoTotal) 
	{
		if(_costoTotal >= 0 && km > 0) 
			costoTotal = km * _costoTotal;
		else
			throw new IllegalArgumentException("No se aceptan costos negativos");
	}
	
	public static void setExcedio300Km(boolean Excede) 
	{
		excedioKm=Excede;
	}
		
	public static void setInteconexionProvincias(boolean interconecta) 
	{
		inteconectaDosPrivincias = interconecta;
	}
	
	//GETTERS

	public Integer getCosto300km() 
	{
		return costo300km;
	}

	public Integer getCostoDos() 
	{
		return costoDos;
	}

	public Integer getCostoFijo() 
	{
		return costoFijo;
	}
	
	public static int getCostoTotal() 
	{
		return costoTotal;
	}

	public static boolean getExcedio300Km() 
	{
		return excedioKm ;
	}

	public static boolean getInterconxionProvincias() 
	{
		return inteconectaDosPrivincias;
	}
}
